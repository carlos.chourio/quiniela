﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Quiniela.Model.DTOs.Abstractions {
    public class QuinielaDtoBase {
        public string Nombre { get; set; }
        public virtual ICollection<QuinelaDetalleJugadasDtoBase> QuinelaDetalleJugadas { get; set; } 
            = new Collection<QuinelaDetalleJugadasDtoBase>();
    }
}
