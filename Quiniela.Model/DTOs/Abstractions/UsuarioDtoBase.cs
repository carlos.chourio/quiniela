namespace Quiniela.Model.DTOs.Abstractions {
    public class UsuarioDtoBase {
        public string Correo { get;set; }
        public string Clave { get;set; }
    }
}