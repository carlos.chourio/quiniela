﻿namespace Quiniela.Model.DTOs.Abstractions {
    public class QuinelaDetalleJugadasDtoBase {
        public int Numero { get; set; }
        public string Grupo { get; set; }
        public string PaisLocal { get; set; }
        public int PaisLocalGol { get; set; }
        public string PaisVisitante { get; set; }
        public int PaisVisitanteGol { get; set; }
    }
}