using AutoHateoas.AspNetCore.DTOs;
using AutoHateoas.AspNetCore.Validation;
using System.ComponentModel.DataAnnotations;

namespace Quiniela.Model.DTOs.Abstractions {
    public class PartidoDtoBase {
        [Required(ErrorMessage = "El campo Grupo es obligatorio")]
        public string Grupo { get; set; }
        [Required(ErrorMessage = "El campo País Local es obligatorio")]
        public string PaisLocal { get; set; }
        [Required(ErrorMessage = "El campo Goles País Local es obligatorio")]
        [PositiveInteger(ErrorMessage = "La cantidad de Goles del País Local debe ser positiva")]
        public int PaisLocalGol { get; set; }
        [Required(ErrorMessage = "El campo País Visitante es obligatorio")]
        public string PaisVisitante { get; set; }
        [Required(ErrorMessage = "El campo Goles País Visitante es obligatorio")]
        [PositiveInteger(ErrorMessage = "La cantidad de Goles del País Visitante debe ser positiva")]
        public int PaisVisitanteGol { get; set; }
    }
}