using AutoHateoas.AspNetCore.DTOs;
using Quiniela.Model.DTOs.Abstractions;

namespace  Quiniela.Model.DTOs.Put {
    public class UsuarioDtoPut : UsuarioDtoBase, IIdentityDto {
        public int Id { get;set; }
    }
}