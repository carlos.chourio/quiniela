﻿using Quiniela.Model.DTOs.Abstractions;

namespace Quiniela.Model.DTOs.Put {
    public class QuinielaDtoPut : QuinielaDtoBase {
        public int Id { get;set; }
    }
}