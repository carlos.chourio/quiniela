using AutoHateoas.AspNetCore.DTOs;
using Quiniela.Model.DTOs.Abstractions;

namespace Quiniela.Model.DTOs.Get {

    public class PartidoDtoGet : PartidoDtoBase, IIdentityDto {
        public int Id { get; set; }
    }
}
