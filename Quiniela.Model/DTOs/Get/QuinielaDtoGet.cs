﻿using AutoHateoas.AspNetCore.DTOs;
using Quiniela.Model.DTOs.Abstractions;

namespace Quiniela.Model.DTOs.Get {
    public class QuinielaDtoGet : QuinielaDtoBase, IIdentityDto {
        public int Id { get;set; }
    }
}
