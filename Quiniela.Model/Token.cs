﻿using System;

namespace Quiniela.Model {
    public class Token {
        public string TokenValue { get; set; }
        public DateTime Expiration { get; set; }
        public int UserId { get; set; }
    }
}
