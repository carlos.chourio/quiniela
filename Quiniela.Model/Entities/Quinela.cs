﻿using System;
using System.Collections.Generic;

namespace Quiniela.Model
{
    public partial class Quinela
    {
        public Quinela()
        {
            QuinelaDetalleJugadas = new HashSet<QuinelaDetalleJugadas>();
        }

        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Nombre { get; set; }
        public bool IsDeleted { get; set; }
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }

        public virtual ICollection<QuinelaDetalleJugadas> QuinelaDetalleJugadas { get; set; }
    }
}
