﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Quiniela.Data.Repositories.Abstractions;
using Quiniela.Model;
using Quiniela.Services.Abstractions;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Quiniela.Services {
    public class LoginService : ILoginService {
        private readonly IConfiguration configuration;
        private readonly IContextGenericRepository<Usuarios, DbContext> usuarioRepository;

        public LoginService(IConfiguration configuration, IContextGenericRepository<Usuarios, DbContext> usuarioRepository) {
            this.configuration = configuration;
            this.usuarioRepository = usuarioRepository;
        }

        public async Task<bool> ValidateCredentialsAsync(string email, string password) {
            var usuario = await usuarioRepository.GetBy(t => t.Correo == email).FirstOrDefaultAsync();
            if (usuario != null) {
                return usuario.Clave == password;
            }
            return false;
        }

        public async Task<Usuarios> GetUser(string userName) {
            return await usuarioRepository.GetBy(t=> t.Correo == userName).FirstOrDefaultAsync();
        }

        public async Task<Token> CreateTokenAsync(string userName) {
            var claims = new[] {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.Role, "Manager")
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                configuration["Tokens:Issuer"],
                configuration["Tokens:Audience"],
                claims,
                expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: credentials);
            return new Token {
                TokenValue = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = token.ValidTo,
                UserId = (await GetUser(userName)).Id
            };
        }

        public void Add(Usuarios entity, string userName = null) {
            usuarioRepository.Add(entity);
        }

        public async Task UpdateAsync() {
            await usuarioRepository.UpdateAsync();
        }
    }
}