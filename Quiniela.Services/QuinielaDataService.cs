﻿using AutoHateoas.AspNetCore.Collections.Abstractions;
using AutoHateoas.AspNetCore.Services.Abstractions;
using Microsoft.EntityFrameworkCore;
using Quiniela.Data.Repositories.Abstractions;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;
using Quiniela.Services.Abstractions;
using System.Linq;
using System.Threading.Tasks;
using AutoHateoas.AspNetCore.Extensions;
using System;

namespace Quiniela.Services {
    public class QuinielaDataService : IQuinielaDataService {
        private readonly IContextGenericRepository<Quinela, DbContext> quinielaRepository;
        private IPropertyMappingService propertyMappingService;
        private readonly ILoginService loginService;

        public QuinielaDataService(IContextGenericRepository<Quinela, DbContext> quinielaRepository, IPropertyMappingService propertyMappingService, ILoginService loginService) {
            this.quinielaRepository = quinielaRepository;
            this.propertyMappingService = propertyMappingService;
            this.loginService = loginService;
        }

        public async Task<Quinela> GetAsync(int userId, int quinielaId) {
            return await quinielaRepository.GetBy(t => !t.IsDeleted && t.Id == quinielaId && t.CreatorUserId == userId).Include(t => t.QuinelaDetalleJugadas).FirstOrDefaultAsync();
        }

        public async Task AddAsync(Quinela entity, string userName = null) {
            entity.CreationTime = DateTime.Now;
            if (!string.IsNullOrEmpty(userName)) {
                entity.CreatorUserId = (await loginService.GetUser(userName)).Id;
            }
            quinielaRepository.Add(entity);
        }

        public async Task UpdateAsync() {
            await quinielaRepository.UpdateAsync();
        }

        public async Task<IPagedList<Quinela>> GetPageAsync(int pageNumber, int pageSize, string searchQuery, string orderBy, int[] quinielaIds = null) {
            IQueryable<Quinela> items = (quinielaIds != null) 
                ? quinielaRepository.GetBy(t => !t.IsDeleted && quinielaIds.Contains(t.Id)).Include(t => t.QuinelaDetalleJugadas) 
                : quinielaRepository.GetBy(t => !t.IsDeleted).Include(t=> t.QuinelaDetalleJugadas);
            return await GetPageAsync(items, pageNumber, pageSize, searchQuery, orderBy);
        }

        public async Task<IPagedList<Quinela>> GetPageByUserAsync(int userId, int pageNumber, int pageSize, string searchQuery, string orderBy) {
            IQueryable<Quinela> items = quinielaRepository.GetBy(t => !t.IsDeleted && t.CreatorUserId == userId).Include(t => t.QuinelaDetalleJugadas);
            var page = await GetPageAsync(items, pageNumber, pageSize, searchQuery, orderBy);
            return page;
        }

        private async Task<IPagedList<Quinela>> GetPageAsync(IQueryable<Quinela> filteredItems, int pageNumber, int pageSize, string searchQuery, string orderBy) {
            if (string.IsNullOrEmpty(orderBy)) orderBy = "Nombre";
            if (!string.IsNullOrEmpty(searchQuery)) {
                filteredItems = filteredItems.Where(t => t.Nombre.Contains(searchQuery.Trim()));
            }
            return await filteredItems.OrderBy(orderBy, propertyMappingService.GetMapping<QuinielaDtoGet, Quinela>()).
                        ToPagedListAsync(pageSize, pageNumber);
        }

        public IQueryable<Quinela> GetQuinielasByUser(int userId, string searchQuery, string orderBy) {
            IQueryable<Quinela> items = quinielaRepository.GetBy(t => t.CreatorUserId == userId);
            if (string.IsNullOrEmpty(orderBy)) {
                orderBy = "Nombre";
            }
            if (!string.IsNullOrEmpty(searchQuery)) {
                items = items.Where(t => t.Nombre.Contains(searchQuery.Trim()));
            }
            return items.OrderBy(orderBy, propertyMappingService.GetMapping<QuinielaDtoGet, Quinela>());
        }

        public IQueryable<Quinela> GetQuinielas(string searchQuery, string orderBy) {
            IQueryable<Quinela> items = quinielaRepository.GetAll();
            if (string.IsNullOrEmpty(orderBy)) {
                orderBy = "Nombre";
            }
            if (!string.IsNullOrEmpty(searchQuery)) {
                items = items.Where(t => t.Nombre.Contains(searchQuery.Trim()));
            }
            return items.OrderBy(orderBy, propertyMappingService.GetMapping<QuinielaDtoGet, Quinela>());
        }
    }
}
