using System;
using System.Threading.Tasks;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;
using Quiniela.Data.Repositories.Abstractions;
using Microsoft.EntityFrameworkCore;
using AutoHateoas.AspNetCore.Services.Abstractions;
using AutoHateoas.AspNetCore.Collections.Abstractions;
using AutoHateoas.AspNetCore.Extensions;
using System.Linq;
using Quiniela.Services.Abstractions;

namespace Quiniela.Services {
    public class PartidoDataService : IPartidoDataService {
        private readonly IPropertyMappingService propertyMappingService;
        private readonly IContextGenericRepository<Partido, DbContext> partidoRepository;
        private readonly ILoginService loginService;

        public PartidoDataService(IPropertyMappingService propertyMappingService, IContextGenericRepository<Partido,DbContext> partidoRepository, ILoginService loginService) {
            this.propertyMappingService = propertyMappingService;
            this.partidoRepository = partidoRepository;
            this.loginService = loginService;
        }

        public async Task AddAsync(Partido entity, string userName) {
            entity.CreationTime = DateTime.Now;
            if (!string.IsNullOrEmpty(userName)) {
                entity.CreatorUserId = (await loginService.GetUser(userName)).Id;
            }
            partidoRepository.Add(entity);
        }

        public async Task<Partido> GetAsync(int id) {
            return await partidoRepository.GetBy(t=> !t.IsDeleted && t.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IPagedList<Partido>> GetPageAsync(int pageNumber, int pageSize, string searchQuery, string orderBy) {
            IQueryable<Partido> items = partidoRepository.GetBy(t=> !t.IsDeleted);
            if (string.IsNullOrEmpty(orderBy)) orderBy = "PaisLocal";
            if (!string.IsNullOrEmpty(searchQuery)) {
                items = items.Where(t => t.PaisLocal.CaseContains(searchQuery, StringComparison.CurrentCultureIgnoreCase) ||
                        t.PaisVisitante.CaseContains(searchQuery, StringComparison.CurrentCultureIgnoreCase));
            }
            return await items.OrderBy(orderBy, propertyMappingService.GetMapping<PartidoDtoGet, Partido>()).
                        ToPagedListAsync(pageSize, pageNumber);
        }

        public async Task UpdateAsync() {
            await partidoRepository.UpdateAsync();
        }
    }
}