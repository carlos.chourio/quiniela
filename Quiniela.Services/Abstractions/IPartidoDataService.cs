﻿using System.Threading.Tasks;
using AutoHateoas.AspNetCore.Collections.Abstractions;
using Quiniela.Model;

namespace Quiniela.Services.Abstractions {
    public interface IPartidoDataService  {
        Task AddAsync(Partido entity, string userName);
        Task<Partido> GetAsync(int id);
        Task<IPagedList<Partido>> GetPageAsync(int pageNumber, int pageSize, string searchQuery, string orderBy);
        Task UpdateAsync();
    }
}