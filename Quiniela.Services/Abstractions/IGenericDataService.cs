﻿using System.Threading.Tasks;

namespace Quiniela.Services.Abstractions {
    public interface IGenericDataService<TEntity> {
        void Add(TEntity entity, string userName = null);
        Task UpdateAsync();
    }
}
