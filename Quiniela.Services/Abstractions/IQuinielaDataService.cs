﻿using System.Linq;
using System.Threading.Tasks;
using AutoHateoas.AspNetCore.Collections.Abstractions;
using Quiniela.Model;

namespace Quiniela.Services.Abstractions {
    public interface IQuinielaDataService {
        Task AddAsync(Quinela entity, string userName = null);
        Task<Quinela> GetAsync(int userId, int quinielaId);
        IQueryable<Quinela> GetQuinielasByUser(int userId, string searchQuery, string orderBy);
        IQueryable<Quinela> GetQuinielas(string searchQuery, string orderBy);
        Task<IPagedList<Quinela>> GetPageByUserAsync(int userId, int pageNumber, int pageSize, string searchQuery, string orderBy);
        Task<IPagedList<Quinela>> GetPageAsync(int pageNumber, int pageSize, string searchQuery, string orderBy, int[] quinielaIds = null);
        Task UpdateAsync();
    }
}