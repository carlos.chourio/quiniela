﻿using Quiniela.Model;
using System.Threading.Tasks;

namespace Quiniela.Services.Abstractions {
    public interface ILoginService : IGenericDataService<Usuarios> {
        Task<bool> ValidateCredentialsAsync(string userName, string password);
        Task<Token> CreateTokenAsync(string userName);
        Task<Usuarios> GetUser(string userName);
    }
}