﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Quiniela.Data.Repositories.Abstractions {
    public interface IGenericRepository<TModel> {
        void Add(TModel entity);
        void Delete(TModel entity);
        Task<TModel> GetAsync(int id);
        IQueryable<TModel> GetAll();
        IQueryable<TModel> GetBy(Expression<Func<TModel, bool>> expression);
        Task UpdateAsync();
    }
}