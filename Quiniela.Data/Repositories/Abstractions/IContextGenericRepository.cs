﻿namespace Quiniela.Data.Repositories.Abstractions
{
    public interface IContextGenericRepository<TModel,TContext> : IGenericRepository<TModel> {
    }
}