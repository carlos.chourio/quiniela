﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Quiniela.Data.Repositories.Abstractions {
    public class BaseGenericRepository<TModel, TContext> : IContextGenericRepository<TModel, TContext>
        where TContext : DbContext
        where TModel : class {
        protected readonly TContext context;

        public BaseGenericRepository(TContext context) {
            this.context = context;
        }

        public virtual void Add(TModel entity) {
            context.Set<TModel>().Add(entity);
        }

        public virtual void Delete(TModel entity) {
            context.Set<TModel>().Remove(entity);
        }

        public virtual async Task<TModel> GetAsync(int id) {
            return await context.Set<TModel>().FindAsync(id);
        }

        public virtual IQueryable<TModel> GetAll() {
            return context.Set<TModel>();
        }

        public virtual IQueryable<TModel> GetBy(Expression<Func<TModel, bool>> expression) {
            return context.Set<TModel>().Where(expression);
        }

        public async Task UpdateAsync() {
            await context.SaveChangesAsync();
        }
    }
}