﻿using System.Linq;
using AspNetCoreRateLimit;
using AutoMapper;
using AutoHateoas.AspNetCore.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quiniela.API.Mapping;
using AutoHateoas.AspNetCore.Common;
using AutoHateoas.AspNetCore.Filters;

namespace Quiniela.API {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddRepos();
            services.AddAllCors();
            services.AddAutoMapper();
            services.AddLoginService();
            services.AddDataServices();
            services.AddJwtTokenAuthentication(Configuration);
            services.AddMvc(t => {
                t.ReturnHttpNotAcceptable = true;
                t.RespectBrowserAcceptHeader = true;
                var jsonOutputFormatter = t.OutputFormatters.OfType<JsonOutputFormatter>().FirstOrDefault();
                jsonOutputFormatter?.SupportedMediaTypes.Add("application/vnd.quiniela.hateoas+json");
                //var jsonInputFormatter = t.InputFormatters.OfType<JsonInputFormatter>().FirstOrDefault();
                //jsonInputFormatter?.SupportedMediaTypes.Add("application/vnd.quiniela.hateoas+json");
            })
                .AddJsonOptions(options => options.UseCamelCasing(true));
            services.AddContext(Configuration.GetConnectionString("QuinielaConnectionString"));
            services.AddUrlMetadataHelper();
            services.AddPropertyMappingService();
            services.AddPropertyValidationService<PropertyMappingProfile>();
            services.AddHttpCacheHeaders(t => {
                t.MaxAge = 500;
            },
            y => {
                y.MustRevalidate = true;
            });
            services.AddAutoHateoas();
            services.AddResponseCaching();
            services.AddMemoryCache();
            services.AddRateLimiting();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseExceptionHandler(t => t.Run(y=> {
                    y.Response.StatusCode= 500;
                    return null;
                }));
            }
            Mapper.Initialize(cfg => {
                cfg.AddProfile<AutomapperMappingProfile>();
            });
            app.UseIpRateLimiting();
            app.UseCors("EnableCORS");
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseResponseCaching();
            app.UseHttpCacheHeaders();
            //app.UseAutoHateoas<SamplePaginationModel>(typeof(Startup).Assembly,);
            app.UseAutoHateoas(typeof(Startup).Assembly,
                new HateoasConfiguration() {
                    CustomDataType = "application/vnd.quiniela.hateoas+json",
                    CustomPaginationModelTypes = new System.Type[] {
                        typeof(SamplePaginationModel), typeof(SamplePaginationModel<>)
                    }
            });
            app.UseMvcWithDefaultRoute();
        }
    }
}