﻿using AutoMapper;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;
using Quiniela.Model.DTOs.Post;

namespace Quiniela.API.Mapping {
    public class AutomapperMappingProfile : Profile {
        public AutomapperMappingProfile() {
            CreateMappingProfile();
        }

        private void CreateMappingProfile() {
            CreateMapForPartido();
            CreateMapForQuiniela();
            // Add more mappings for other entities
        }

        private void CreateMapForQuiniela() {
            CreateMap<QuinielaDtoPost, Quinela>(MemberList.Destination);
            CreateMap<Quinela, QuinielaDtoGet>(MemberList.Destination);
        }

        private void CreateMapForPartido() {
            CreateMap<PartidoDtoPost, Partido>(MemberList.Destination);
            CreateMap<Partido, PartidoDtoGet>(MemberList.Destination);
        }
    }
}
