using System;
using System.Collections.Generic;
using AutoHateoas.AspNetCore.Common;
using AutoHateoas.AspNetCore.Extensions;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;

namespace Quiniela.API.Mapping {
    public class PropertyMappingProfile : MappingProfile {
        private IDictionary<string, PropertyMappingValue> partidoPropertyMapping = 
            new Dictionary<string,PropertyMappingValue>(StringComparer.CurrentCultureIgnoreCase);
        private IDictionary<string, PropertyMappingValue> quinielaPropertyMapping =
            new Dictionary<string, PropertyMappingValue>(StringComparer.CurrentCultureIgnoreCase);
        public PropertyMappingProfile() {
            CreateMappingForPartido();
            CreateMappingForQuiniela();
        }

        private void CreateMappingForPartido() {
            partidoPropertyMapping
                .AddMappingValue("PaisVisitante", false, "PaisVisitante")
                .AddMappingValue("PaisVisitanteGol", false, "PaisVisitanteGol")
                .AddMappingValue("PaisLocal", false, "PaisLocal")
                .AddMappingValue("PaisLocalGol", false, "PaisLocalGol");
            AddMappingDictionary<PartidoDtoGet, Partido>(partidoPropertyMapping);
        }

        private void CreateMappingForQuiniela() {
            partidoPropertyMapping
                .AddMappingValue("Nombre", false, "Nombre");
            AddMappingDictionary<QuinielaDtoGet, Quinela>(partidoPropertyMapping);
        }
    }
}