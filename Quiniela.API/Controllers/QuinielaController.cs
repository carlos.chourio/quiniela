﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Quiniela.Model.DTOs.Abstractions;
using Quiniela.Model.DTOs.Get;
using System;
using System.Threading.Tasks;
using Quiniela.Model;
using Quiniela.Services.Abstractions;
using Quiniela.Model.DTOs.Post;
using Quiniela.Model.DTOs.Put;
using Microsoft.AspNetCore.JsonPatch;
using AutoHateoas.AspNetCore.Common;
using AutoHateoas.AspNetCore.Services.Abstractions;
using AutoHateoas.AspNetCore.Filters;
using AutoHateoas.AspNetCore.Attributes;
using System.Collections.Generic;
using AutoHateoas.AspNetCore;

namespace Quiniela.API.Controllers {
    [ApiController]
    [Route("api/Quiniela")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class QuinielaController : ControllerBase {
        private readonly IQuinielaDataService quinielaDataService;
        private readonly IMapper mapper;
        private readonly IPropertyMappingService propertyMappingService;
        private readonly ILoginService loginService;

        public QuinielaController(IQuinielaDataService quinielaDataService, IMapper mapper, 
            IPropertyMappingService propertyMappingService, ILoginService loginService) {
            this.quinielaDataService = quinielaDataService;
            this.mapper = mapper;
            this.propertyMappingService = propertyMappingService;
            this.loginService = loginService;
        }

        [HttpGet("{id:int}", Name="GetQuiniela")]
        [HateoasResource(ResourceType.Single)]
        [ServiceFilter(typeof(Hateoas<QuinielaDtoGet>))]
        public async Task<IActionResult> GetQuiniela(int id) {
            int userId = await GetCurrentUserIdAsync();
            var quiniela = await quinielaDataService.GetAsync(userId, id);
            if (quiniela != null) {
                return Ok(mapper.Map<QuinielaDtoGet>(quiniela));
            }
            return NotFound();
        }

        [HttpGet(Name = "GetQuinielasByUser")]
        [HateoasResource(ResourceType.Collection)]
        [ServiceFilter(typeof(HateoasForCollection<Quinela, QuinielaDtoGet>))]
        public async Task<IActionResult> GetQuinielasByUser([FromQuery] SamplePaginationModel paginationViewModel) {
            if (propertyMappingService.ValidateMappingsForOrderBy<QuinielaDtoGet, Quinela>(paginationViewModel.OrderBy)) {
                int userId = await GetCurrentUserIdAsync();
                var quinielas = await quinielaDataService.GetPageByUserAsync(userId, paginationViewModel.PageNumber, paginationViewModel.PageSize, paginationViewModel.SearchQuery, paginationViewModel.OrderBy);
                if (quinielas != null) {
                    return new PaginatedResult(
                        mapper.Map<IEnumerable<QuinielaDtoGet>>(quinielas),
                        new PaginationInfo(quinielas.TotalCount, quinielas.PageSize, quinielas.CurrentPage, quinielas.TotalPages));
                }
                return NotFound();
            }
            return BadRequest("Alguno(s) de los campos solicitados no existe(n)");
        }

        [HttpPost(Name="PostQuiniela")]
        [HateoasResource(ResourceType.Single)]
        [ServiceFilter(typeof(Hateoas<QuinielaDtoGet>))]
        public async Task<IActionResult> PostQuiniela(QuinielaDtoPost quinielaDto) {
            if (quinielaDto!=null) {
                if (ModelState.IsValid) {
                    var quiniela = mapper.Map<Quinela>(quinielaDto);
                    await quinielaDataService.AddAsync(quiniela, User.Identity.Name);
                    await quinielaDataService.UpdateAsync();
                    CreatedAtRoute("GetQuiniela", new { quiniela.Id }, mapper.Map<QuinielaDtoGet>(quiniela));
                }
                return new UnprocessableEntityObjectResult(ModelState);
            }
            return BadRequest();
        }

        [HttpPut(Name = "PutQuiniela")]
        [HateoasResource(ResourceType.Single)]
        [ServiceFilter(typeof(Hateoas<QuinielaDtoGet>))]
        public async Task<IActionResult> PutQuiniela(QuinielaDtoPut quinielaDto) {
            var quiniela = mapper.Map<Quinela>(quinielaDto);
            quiniela.LastModificationTime = DateTime.Now;
            await quinielaDataService.UpdateAsync();
            return Ok(mapper.Map<QuinielaDtoGet>(quiniela));
        }

        [HttpPatch("{id:int}", Name = "PatchQuiniela")]
        [HateoasResource(ResourceType.Single)]
        [ServiceFilter(typeof(Hateoas<QuinielaDtoGet>))]
        public async Task<IActionResult> PartialUpdateQuiniela(int id, JsonPatchDocument<QuinielaDtoBase> quinielaPatch) {
            int userId = await GetCurrentUserIdAsync();
            var quiniela = await quinielaDataService.GetAsync(userId, id);
            if (quiniela!=null) {
                if (ModelState.IsValid) {
                    var quinielaBaseDto = mapper.Map<QuinielaDtoBase>(quiniela);
                    quinielaPatch.ApplyTo(quinielaBaseDto);
                    quiniela = mapper.Map<Quinela>(quinielaBaseDto);
                    await quinielaDataService.UpdateAsync();
                    return Ok(mapper.Map<QuinielaDtoGet>(quiniela));
                }
                return new UnprocessableEntityObjectResult(ModelState);   
            }
            return NotFound();
        }

        private async Task<int> GetCurrentUserIdAsync() {
            string userName = User.Identity.Name;
            int userId = (await loginService.GetUser(userName)).Id;
            return userId;
        }
    }
}
