﻿using Microsoft.AspNetCore.Mvc;
using Quiniela.Model;
using Quiniela.Services.Abstractions;
using System.Threading.Tasks;

namespace Quiniela.API.Controllers {
    [ApiController]
    [Route("api/Login")]
    public class LoginController : ControllerBase {
        private readonly ILoginService loginService;

        public LoginController(ILoginService loginService) {
            this.loginService = loginService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginModel loginViewModel) {
            if (ModelState.IsValid) {
                if (await loginService.ValidateCredentialsAsync(loginViewModel.UserName, loginViewModel.Password)) {
                    Token token = await loginService.CreateTokenAsync(loginViewModel.UserName);
                    return Created("", token);
                }
                return NotFound("El usuario o contraseña no existen");
            }
            return BadRequest("Ocurrio un error. Intenta más tarde");
        }
    }
}