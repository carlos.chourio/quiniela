using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;
using Quiniela.Model.DTOs.Post;
using Quiniela.Model.DTOs.Put;
using Quiniela.Services.Abstractions;
using System;
using System.Threading.Tasks;
using Quiniela.Model.DTOs.Abstractions;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using AutoHateoas.AspNetCore.Services.Abstractions;
using AutoHateoas.AspNetCore.Common;
using AutoHateoas.AspNetCore.Filters;

namespace Quiniela.API.Controllers {
    [ApiController]
    [Route("api/partido")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PartidoController : ControllerBase {
        private readonly IPartidoDataService partidoDataService;
        private readonly IMapper mapper;
        private readonly IPropertyMappingService propertyMappingService;        

        public PartidoController(IPartidoDataService partidoDataService, IMapper mapper, IPropertyMappingService propertyMappingService) {
            this.partidoDataService = partidoDataService;
            this.mapper = mapper;
            this.propertyMappingService = propertyMappingService;
        }

        [HttpHead]
        [HttpGet("{id:int}", Name = "GetPartido")]
        [ServiceFilter(typeof(Hateoas<PartidoDtoGet>))]
        public async Task<IActionResult> GetPartido(int id) {
            var partido = await partidoDataService.GetAsync(id);
            if (partido != null) {
                var partidoDto = mapper.Map<Partido, PartidoDtoGet>(partido);
                return Ok(partidoDto);   
            }
            return NotFound();
        }

        [HttpPut(Name="PutPartido")]
        [ServiceFilter(typeof(Hateoas<PartidoDtoGet>))]
        public async Task<IActionResult> PutPartido([FromBody] PartidoDtoPut partidoDto) {
            if (partidoDto != null) {
                var partido = mapper.Map<PartidoDtoPut, Partido>(partidoDto);
                partido.LastModificationTime = DateTime.Now;
                await partidoDataService.UpdateAsync();
                var partidoDtoGet = mapper.Map<Partido, PartidoDtoGet>(partido);
                return Ok(partidoDtoGet);
            }
            return BadRequest();
        }

        [HttpPost(Name = "PostPartido")]
        [ServiceFilter(typeof(Hateoas<PartidoDtoGet>))]
        public async Task<IActionResult> PostPartido([FromBody] PartidoDtoPost partidoDto) {
            if (partidoDto != null) {
                var partido = mapper.Map<PartidoDtoPost, Partido>(partidoDto);
                partido.CreationTime = DateTime. Now;
                await partidoDataService.AddAsync(partido, User.Identity.Name);
                await partidoDataService.UpdateAsync();
                var partidoDtoGet = mapper.Map<Partido, PartidoDtoGet>(partido);
                return CreatedAtRoute("GetPartido", new { partido.Id }, partidoDtoGet);
            }
            return BadRequest();
        }

        [HttpDelete("{id:int}", Name="DeletePartido")]
        public async Task<IActionResult> DeletePartido(int id) {
            var partidoToDelete = await partidoDataService.GetAsync(id);
            if (partidoToDelete != null) {
                partidoToDelete.IsDeleted = true;
                partidoToDelete.DeletionTime = DateTime.Now;
                await partidoDataService.UpdateAsync();
                return Ok();
            }
            return BadRequest("El partido que está tratando de eliminar, no existe");
        }

        [HttpHead]
        [HttpGet(Name = "GetPartidos")]
        [ServiceFilter(typeof(HateoasForCollection<Partido, PartidoDtoGet>))]
        public async Task<IActionResult> GetPartidos([FromQuery] ValidatedPaginationModel<Partido> paginationViewModel) {
            if (propertyMappingService.ValidateMappingsForOrderBy<PartidoDtoGet, Partido>(paginationViewModel.OrderBy)) {
                var partidos = await partidoDataService.GetPageAsync(paginationViewModel.PageNumber, paginationViewModel.PageSize, paginationViewModel.SearchQuery, paginationViewModel.OrderBy);
                if (partidos != null && partidos.Count>0) {
                    var partidosDto = mapper.Map<IEnumerable<PartidoDtoGet>>(partidos);
                    return Ok(partidosDto);
                }
                return NotFound();              
            }
            return BadRequest("La clausula order by es incorrecta.");
        }

        [HttpPatch("{id:int}")]
        [ServiceFilter(typeof(Hateoas<PartidoDtoGet>))]
        public async Task<IActionResult> PartialUpdatePartidos(int id, [FromBody]JsonPatchDocument<PartidoDtoBase> partidoPatchDocument, [FromHeader(Name = "Accept")]string mediaType) {
            var partido = await partidoDataService.GetAsync(id);
            if (partido != null) {
                if (ModelState.IsValid) {
                    var partidoDto = mapper.Map<PartidoDtoBase>(partido);
                    partidoPatchDocument.ApplyTo(partidoDto);
                    partido = mapper.Map<Partido>(partidoDto);
                    await partidoDataService.UpdateAsync();
                    var partidoDtoGet = mapper.Map<PartidoDtoGet>(partido);
                    return Ok(partidoDtoGet);
                }
                return new UnprocessableEntityObjectResult(ModelState);
            }
            return NotFound();
        }

        [HttpOptions]
        public IActionResult GetOptions() {
            Response.Headers.Add("Allow", "GET, POST, PUT, PATCH, OPTIONS, HEAD");
            return Ok();
        }
    }
}