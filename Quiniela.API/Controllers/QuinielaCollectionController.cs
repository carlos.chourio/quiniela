﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Quiniela.Model;
using Quiniela.Model.DTOs.Get;
using Quiniela.Services.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoHateoas.AspNetCore.Services.Abstractions;
using AutoHateoas.AspNetCore.Common;
using AutoHateoas.AspNetCore.Attributes;
using AutoHateoas.AspNetCore.Filters;
using AutoHateoas.AspNetCore;

namespace Quiniela.API.Controllers {
    [ApiController]
    [Route("api/QuinielaCollection")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class QuinielaCollectionController : ControllerBase {
        private readonly IPropertyMappingService propertyMappingService;
        private readonly IQuinielaDataService quinielaDataService;
        private readonly IMapper mapper;

        public QuinielaCollectionController(IPropertyMappingService propertyMappingService, 
            IQuinielaDataService quinielaDataService, IMapper mapper) {
            this.propertyMappingService = propertyMappingService;
            this.quinielaDataService = quinielaDataService;
            this.mapper = mapper;
        }

        [HttpHead]
        [HateoasResource(ResourceType.Collection)]
        [ServiceFilter(typeof(HateoasForCollection<Quinela,QuinielaDtoGet>))]
        [HttpGet(Name = "GetQuinielas")]
        public async Task<IActionResult> GetQuinielaPage([FromQuery] ValidatedPaginationModel<Quinela> paginationViewModel) {
            if (ModelState.IsValid) {
                if (propertyMappingService.ValidateMappingsForOrderBy<QuinielaDtoGet, Quinela>(paginationViewModel.OrderBy)) {
                    var quinielaPage = await quinielaDataService.GetPageAsync(paginationViewModel.PageNumber, paginationViewModel.PageSize, paginationViewModel.SearchQuery, paginationViewModel.OrderBy, paginationViewModel.Ids);
                    if (quinielaPage != null) {
                        var quinielasDto = mapper.Map<IEnumerable<QuinielaDtoGet>>(quinielaPage);
                        return new PaginatedResult(quinielasDto, new PaginationInfo(
                            quinielaPage.TotalCount, quinielaPage.PageSize, quinielaPage.CurrentPage, quinielaPage.TotalPages));
                    }
                    return NotFound();
                }
                return BadRequest("Alguno(s) de los campos solicitados no existe(n)");
            }
            return BadRequest("La clausula order by es incorrecta.");
        }

        public IActionResult GetOptions() {
            Response.Headers.Add("Allow", "GET, OPTIONS, HEAD");
            return Ok();
        }
    }
}
