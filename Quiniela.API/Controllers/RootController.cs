using System.Collections.Generic;
using AutoHateoas.AspNetCore.DTOs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace Quiniela.API {
    [Controller]
    [Route("api")]
    [ApiController]
    public class RootController : ControllerBase {
        private readonly LinkGenerator linkGenerator;

        public RootController(LinkGenerator linkGenerator) {
            this.linkGenerator = linkGenerator;
        }

        [HttpGet]
        public IActionResult GetDocument([FromHeader]string mediaType) {
            if (mediaType.Equals("application/vdn.quiniela.hateoas+json")) {
                var links = new List<LinkDto>() {
                    new LinkDto(linkGenerator.GetPathByAction("GetQuinielas","Quiniela"),"get-quinielas","GET"),
                    new LinkDto(linkGenerator.GetPathByAction("GetPartidos","Partido"),"get-partidos","GET"),
                    new LinkDto(linkGenerator.GetPathByAction("GetDocument","Root"),"get-document","GET")
                };
                return Ok(links);
            }
            return NoContent();
        }
    }
}