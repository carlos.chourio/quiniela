using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quiniela.Model;
using Quiniela.Model.DTOs.Post;
using Quiniela.Model.DTOs.Put;
using Quiniela.Services.Abstractions;

namespace Quiniela.API.Controllers {
    [ApiController]
    [Route("api/usuario")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UsuarioController : ControllerBase {
        private readonly IMapper mapper;
        private readonly ILoginService loginService;

        public UsuarioController(IMapper mapper, ILoginService loginService) {
            this.mapper = mapper;
            this.loginService = loginService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] UsuarioDtoPost usuarioDto) {
            if (usuarioDto!=null) {
                if (ModelState.IsValid) {
                    var usuario = mapper.Map<UsuarioDtoPost, Usuarios>(usuarioDto);
                    loginService.Add(usuario);
                    await loginService.UpdateAsync();
                }
                return new UnprocessableEntityObjectResult(ModelState);
            }
            return BadRequest("Debe introducir un usuario válido");
        }

        [HttpPut]
        public async Task<IActionResult> ModifyUser([FromBody] UsuarioDtoPut usuarioDto){
            if (usuarioDto!=null) {
                if (ModelState.IsValid) {
                    var usuario = mapper.Map<UsuarioDtoPut, Usuarios>(usuarioDto);
                    await loginService.UpdateAsync();
                }
                return new UnprocessableEntityObjectResult(ModelState.IsValid);
            }
            return BadRequest();
        }
    }
}