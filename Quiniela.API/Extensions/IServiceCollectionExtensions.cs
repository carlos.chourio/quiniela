﻿using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Quiniela.Data;
using Quiniela.Data.Repositories.Abstractions;
using Quiniela.Services;
using Quiniela.Services.Abstractions;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.Extensions.DependencyInjection {
    public static class IServiceCollectionExtensions {
        public static IServiceCollection AddContext(this IServiceCollection serviceCollection, string connectionString) {
            serviceCollection.AddTransient<DbContext, QuinielaWebContext>();
            serviceCollection.AddDbContext<QuinielaWebContext>(t => t.UseSqlServer(connectionString));
            return serviceCollection;
        }
        public static IServiceCollection AddRepos(this IServiceCollection serviceCollection) {
            serviceCollection.AddTransient(typeof(IContextGenericRepository<,>), typeof(BaseGenericRepository<,>));
            return serviceCollection;
        }
        public static IServiceCollection AddLoginService(this IServiceCollection serviceCollection) {
            serviceCollection.AddTransient<ILoginService, LoginService>();
            return serviceCollection;
        }
        public static IServiceCollection AddDataServices(this IServiceCollection serviceCollection) {
            serviceCollection.AddTransient<IPartidoDataService, PartidoDataService>();
            serviceCollection.AddTransient<IQuinielaDataService, QuinielaDataService>();
            return serviceCollection;
        }
        public static IServiceCollection AddUrlMetadataHelper(this IServiceCollection serviceCollection) {
            serviceCollection.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            serviceCollection.AddScoped<IUrlHelper, UrlHelper>(implementationFactory => {
                var actionContext = implementationFactory.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });
            return serviceCollection;
        }
        public static IServiceCollection AddJwtTokenAuthentication(this IServiceCollection serviceCollection, IConfiguration configuration) {
            serviceCollection.AddAuthentication().AddCookie().AddJwtBearer(cfg => {
                cfg.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = configuration["Tokens:Issuer"],
                    ValidAudience = configuration["Tokens:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Tokens:Key"]))
                };
            });
            return serviceCollection;
        }
        public static IServiceCollection AddAllCors(this IServiceCollection serviceCollection) {
            serviceCollection.AddCors(options => {
                options.AddPolicy("EnableCORS", builder => {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials().Build();
                });
            });
            return serviceCollection;
        }
        public static IServiceCollection AddRateLimiting(this IServiceCollection serviceCollection) {
            serviceCollection.Configure<IpRateLimitOptions>(config => {
                config.EnableEndpointRateLimiting = true;
                config.GeneralRules = new List<RateLimitRule>() {
                    new RateLimitRule() { Endpoint = "*", Limit = 100, Period = "1h" }, //works
                    new RateLimitRule() { Endpoint = "*:/api/partido", Limit = 1, Period= "1m" } // doesn't work
                };
            });
            serviceCollection.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            serviceCollection.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            serviceCollection.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            serviceCollection.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            return serviceCollection;
        }
    }
}